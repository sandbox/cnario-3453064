<?php

namespace Drupal\commerce_squad\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_payment\Annotation\CommercePaymentGateway;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayBase;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\RequestException;

/**
 * Provides the Squad Payment Gateway.
 *
 * @CommercePaymentGateway(
 *   id = "squad",
 *   label = @Translation("Squad Payment Gateway"),
 *   display_label = @Translation("Squad"),
 *   modes = {
 *     "sandbox" = @Translation("Sandbox"),
 *     "live" = @Translation("Live"),
 *   },
 *   forms = {
 *     "offsite-payment" = "Drupal\commerce_squad\PluginForm\SquadForm",
 *   },
 *   payment_method_types = {"credit_card"},
 *   credit_card_types = {
 *     "mastercard", "visa",
 *   },
 *   requires_billing_information = FALSE,
 * )
 */
class Squad extends OffsitePaymentGatewayBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'mode' => 'sandbox',
      'currency' => 'NGN',
      'initiate_type' => 'inline',
      'pass_charge' => FALSE,
      'live_url' => 'https://api-d.squadco.com',
      'sandbox_url' => 'https://sandbox-api-d.squadco.com',
      'display_label' => 'Squad Payment Gateway',
      'collect_billing_information' => TRUE,
      'merchant_id' => '',
      'private_key' => '',
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['mode'] = [
      '#type' => 'radios',
      '#title' => $this->t('Mode'),
      '#description' => $this->t('Select the mode for Squad (sandbox or live).'),
      '#default_value' => $this->configuration['mode'],
      '#options' => [
        'sandbox' => $this->t('Sandbox'),
        'live' => $this->t('Live'),
      ],
      '#required' => TRUE,
    ];

    $form['merchant_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Merchant ID'),
      '#description' => $this->t('Enter your Squad Merchant ID.'),
      '#default_value' => $this->configuration['merchant_id'],
      '#required' => TRUE,
    ];

    $form['private_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Private Key'),
      '#description' => $this->t('Enter your Squad API Private Key.'),
      '#default_value' => $this->configuration['private_key'],
      '#required' => TRUE,
    ];

    $form['pass_charge'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Pass Charge'),
      '#description' => $this->t('Whether to pass charges to the customer.'),
      '#default_value' => $this->configuration['pass_charge'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValue($form['#parents']);
    $private_key = $values['private_key'];
    $merchant_id = $values['merchant_id'];

    if (empty($private_key)) {
      $form_state->setErrorByName('private_key', $this->t('The Private Key field cannot be empty.'));
    }

    if (empty($merchant_id)) {
      $form_state->setErrorByName('merchant_id', $this->t('The Merchant ID field cannot be empty.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    $values = $form_state->getValue($form['#parents']);

    // Save the configuration settings.
    $this->configuration['mode'] = $values['mode'];
    $this->configuration['merchant_id'] = $values['merchant_id'];
    $this->configuration['private_key'] = $values['private_key'];
    $this->configuration['pass_charge'] = $values['pass_charge'];
  }

  /**
   * Handles the return from the payment gateway.
   *
   * This method processes the return from the Squad payment gateway after a payment attempt.
   * It verifies the transaction status, updates the order state accordingly, and generates
   * a response indicating the outcome of the transaction.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The incoming request object containing additional data.
   * @param string $order_id
   *   The ID of the order associated with the payment.
   *
   * @return array
   *   An array representing the response containing the rendered page.
   *   - #theme: The theme hook to use for rendering the page.
   *   - #title: The title of the page.
   *   - content: An array representing the content of the page.
   *     - #markup: The HTML markup for displaying the transaction outcome message.
   *
   * @throws \Exception
   *   Thrown if an error occurs during processing.
   */
  public function onReturn(OrderInterface $order, Request $request) {

    // Retrieve the token from session.
    $pkToken = \Drupal::service('session')->get('squad_form_token');
    // Retrieve the private key associated with the token.
    $private_key = \Drupal::service('session')->get('squad_form_token_private_key');

    $transaction_ref = $request->query->get('reference');  
    $mode = $this->configuration['mode'];

    $status = $this->verifyTransaction($transaction_ref, $private_key, $mode);

    // Handle different transaction statuses accordingly.
    switch ($status) {
        case 'success':
            // Transaction was successful, update the order status.
            $order->getState()->applyTransitionById('place');
            $order->save();
            break;

        case 'failed':
            // Transaction failed, update the order status.
            $order->getState()->applyTransitionById('cancel');
            $order->save();
            break;
    }   

    return [
        '#theme' => 'page',
        '#title' => $status == 'success' ? $this->t('Sucess') : $this->t('Transaction Failed'),
        'content' => [
            '#markup' => $status == 'success' ? $this->t('Your transaction was successful.') : $this->t('Your transaction was not successful. Please try again later.'),
        ],
    ];
  }

  /**
   * Handles the cancellation of the payment.
   *
   * This method processes the cancellation of the payment by the user.
   * It updates the order state to 'canceled' and generates a response
   * indicating that the transaction was cancelled.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The incoming request object containing additional data.
   * @param string $order_id
   *   The ID of the order associated with the payment.
   *
   * @return array
   *   An array representing the response containing the rendered page.
   *   - #theme: The theme hook to use for rendering the page.
   *   - #title: The title of the page.
   *   - content: An array representing the content of the page.
   *     - #markup: The HTML markup for displaying the cancellation message.
   */
  public function onCancel(OrderInterface $order, Request $request) {
    // Handle the cancellation of the payment.
    $order->set('state', 'canceled');
    $order->save();

    return [
        '#theme' => 'page',
        '#title' => $this->t('Cancelled!'),
        'content' => [
            '#markup' => $this->t('Your transaction was cancelled. If you need further assistance, please contact our support team.'),
        ],
    ];
  }

  /**
   * Verifies the transaction with Squad.
   *
   * This method verifies the transaction with the Squad payment gateway.
   * It sends a request to the Squad API to verify the transaction status
   * using the provided transaction reference and private key. Based on
   * the response from the API, it determines the transaction status.
   *
   * @param string $transaction_ref
   *   The transaction reference.
   * @param string $private_key
   *   The private key for authentication.
   * @param string $mode
   *   The mode (sandbox or live).
   *
   * @return string
   *   The transaction status:
   *   - 'success' if the transaction is successful.
   *   - 'failed' if the transaction fails.
   *
   * @throws \GuzzleHttp\Exception\RequestException
   *   Thrown if there is an issue with the request to the Squad API.
   */
  protected function verifyTransaction($transaction_ref, $private_key, $mode) {
    $base_url = $mode === 'live' ? $this->configuration['live_url'] : $this->configuration['sandbox_url'];

    // Initialize Guzzle HTTP client.
    $client = new \GuzzleHttp\Client();

    try {
      // Make a request to the Squad API to verify the transaction status.
      $response = $client->get("$base_url/transaction/verify/$transaction_ref", [
          'headers' => [
              'Authorization' => 'Bearer ' . $private_key,
          ],
      ]);

      $data = json_decode($response->getBody(), TRUE);

      // Check for successful API response and transaction status.
      if ($data['status'] === 200) {
          $transactionStatus = $data['data']['transaction_status'];
          
          if ($transactionStatus === 'success') {
              return 'success';
          }
          elseif ($transactionStatus === 'failed') {
              return 'failed';
          } else {
              // Log unexpected transaction status.
              \Drupal::logger('commerce_squad')->warning('Unexpected transaction status: @status', ['@status' => $transactionStatus]);
              return 'failed';
          }
      } else {
          // Log if the API response status is not 200.
          \Drupal::logger('commerce_squad')->error('Unexpected API response status: @status', ['@status' => $data['status']]);
          return 'failed';
      }
    }
    catch (RequestException $e) {
      watchdog_exception('commerce_squad', $e);
      return 'failed';
    }
  }

  /**
   * Handles the webhook from the payment gateway.
   *
   * This method processes incoming webhook requests from the Squad payment gateway.
   * It verifies the request, extracts the transaction details, updates the order
   * status accordingly, and logs the event.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The incoming request object containing the webhook payload.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   A response object indicating the result of the webhook processing.
   *   - 200 OK if the webhook is processed successfully.
   *   - 401 Unauthorized if the webhook signature verification fails.
   *   - 500 Internal Server Error if any other error occurs during processing.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
   *   Thrown if the order specified in the webhook payload is not found.
   * @throws \Exception
   *   Thrown if an error occurs during processing and no specific exception is caught.
   */
  public function webhook(Request $request) {
    $private_key = $this->configuration['private_key'];
    try {
      $data = json_decode($request->getContent(), TRUE);

      if (!$this->verifyWebhookRequest($request, $private_key)) {
        return new Response('Unauthorized', Response::HTTP_UNAUTHORIZED);
      }

      $transactionId = $data['transaction_id'];
      $status = $data['status'];
      $order = Order::load($data['order_id']);

      if (!$order) {
        throw new \Symfony\Component\HttpKernel\Exception\NotFoundHttpException();
      }

      switch ($status) {
        case 'success':
          $order->set('status', 'completed');
          break;
        case 'failed':
          $order->set('status', 'canceled');
          break;
      }

      $order->save();

      \Drupal::logger('commerce_squad')->log(LogLevel::INFO, 'Webhook received and processed successfully.', [
          'transaction_id' => $transactionId,
          'status' => $status,
          'order_id' => $order->id(),
      ]);

      return new Response('Webhook received and processed successfully.', Response::HTTP_OK);
    } catch (\Exception $e) {
        \Drupal::logger('commerce_squad')->error('Error processing webhook: @message', ['@message' => $e->getMessage()]);
        return new Response('An error occurred while processing the webhook.', Response::HTTP_INTERNAL_SERVER_ERROR);
    }
  }

  /**
   * Verifies the authenticity of the webhook request.
   *
   * This method checks the webhook request's signature to ensure it is from a trusted source.
   * The signature is expected to be present in the 'X-Squad-Signature' header and is validated
   * using the private key configured for the payment gateway.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The incoming request object containing the webhook payload and headers.
   * @param string $private_key
   *   The private key used to generate the expected signature for verification.
   *
   * @return bool
   *   TRUE if the request is verified successfully, FALSE otherwise.
   */
  protected function verifyWebhookRequest(Request $request, $private_key) {
    $signature = $request->headers->get('X-Squad-Signature');
    $payload = $request->getContent();

    $expectedSignature = hash_hmac('sha256', $payload, $private_key);

    return hash_equals($expectedSignature, $signature);
  }
}
 