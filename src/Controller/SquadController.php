<?php

namespace Drupal\commerce_squad\Controller;

use Drupal\commerce_order\Entity\Order;
use Drupal\commerce_payment\PaymentGatewayManager;
use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Drupal\Core\Messenger\MessengerInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class SquadController extends ControllerBase {

  /**
   * The payment gateway manager.
   *
   * @var \Drupal\commerce_payment\PaymentGatewayManager
   */
  protected $paymentGatewayManager;

  /**
   * The messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Constructs a new SquadController object.
   *
   * @param \Drupal\commerce_payment\PaymentGatewayManager $payment_gateway_manager
   *   The payment gateway manager.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   */
  public function __construct(PaymentGatewayManager $payment_gateway_manager, MessengerInterface $messenger) {
    $this->paymentGatewayManager = $payment_gateway_manager;
    $this->messenger = $messenger;
  }

  /**
   * Creates a new instance of the SquadController.
   *
   * This method is used by the dependency injection container to create a new instance
   * of the SquadController, injecting the necessary services.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The service container.
   *
   * @return static
   *   A new instance of the SquadController.
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.commerce_payment_gateway'),
      $container->get('messenger')
    );
  }

  /**
   * Loads an order entity by its ID.
   *
   * This method loads an order entity based on the provided order ID. If the order ID is invalid
   * or the order cannot be found, a NotFoundHttpException is thrown.
   *
   * @param int $order_id
   *   The ID of the order to load.
   *
   * @return \Drupal\commerce_order\Entity\Order
   *   The loaded order entity.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
   *   Thrown if the order ID is invalid or the order is not found.
   */
  protected function loadOrder($order_id) {
    if (!is_numeric($order_id) || $order_id <= 0) {
        throw new NotFoundHttpException();
    }

    $order = Order::load($order_id);

    if (!$order) {
        throw new NotFoundHttpException();
    }

    return $order;
  }

  /**
   * Handles the return from the payment gateway.
   *
   * This method processes the return request from the Squad payment gateway. It verifies
   * the transaction status and updates the order status accordingly. If an error occurs,
   * an error message is displayed, and a log entry is created.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The incoming request object containing the return data.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   A response object indicating the result of the return processing.
   *   - 200 OK if the return is processed successfully.
   *   - 500 Internal Server Error if an error occurs during processing.
   */
  public function onReturn(Request $request) {
    try {
      // Extract transaction_ref from the request.
      $transaction_ref = $request->query->get('reference');
      if (!$transaction_ref) {
        throw new NotFoundHttpException('Transaction reference is missing.');
      }

      // transaction_ref is actually the order ID.
      $order = $this->loadOrder($transaction_ref);
      
      $payment_gateway_plugin = $this->paymentGatewayManager->createInstance('squad');
      return $payment_gateway_plugin->onReturn($order, $request);
    } catch (\Exception $e) {
      $this->messenger->addError($this->t('An error occurred while processing the payment return.'));
      \Drupal::logger('commerce_squad')->error($e->getMessage());
      return new Response('Error', Response::HTTP_INTERNAL_SERVER_ERROR);
    }
  }

  /**
   * Handles the cancellation of the payment.
   *
   * This method processes the cancellation request from the Squad payment gateway. It updates
   * the order status to 'canceled'. If an error occurs, an error message is displayed, and a log
   * entry is created.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The incoming request object containing the cancellation data.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   A response object indicating the result of the cancellation processing.
   *   - 200 OK if the cancellation is processed successfully.
   *   - 500 Internal Server Error if an error occurs during processing.
   */
  public function onCancel(Request $request) {
    try {
      // Extract transaction_ref from the request.
      $transaction_ref = $request->query->get('reference');
      if (!$transaction_ref) {
        throw new NotFoundHttpException('Transaction reference is missing.');
      }

      // transaction_ref is actually the order ID.
      $order = $this->loadOrder($transaction_ref);

      $payment_gateway_plugin = $this->paymentGatewayManager->createInstance('squad');
      return $payment_gateway_plugin->onCancel($order, $request);
    } catch (\Exception $e) {
      $this->messenger->addError($this->t('An error occurred while processing the payment cancellation.'));
      \Drupal::logger('commerce_squad')->error($e->getMessage());
      return new Response('Error', Response::HTTP_INTERNAL_SERVER_ERROR);
    }
  }

  /**
   * Handles the webhook from the payment gateway.
   *
   * This method processes the webhook request received from the Squad payment gateway.
   * It delegates the processing to the appropriate payment gateway plugin. If an error
   * occurs during processing, it logs the error and returns a 500 Internal Server Error
   * response.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The incoming request object containing the webhook data.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   A response object indicating the result of the webhook processing.
   *   - 200 OK if the webhook is processed successfully.
   *   - 500 Internal Server Error if an error occurs during processing.
   */
  public function webhook(Request $request) {
    try {
      $payment_gateway_plugin = $this->paymentGatewayManager->createInstance('squad');
      return $payment_gateway_plugin->webhook($request);
    } catch (\Exception $e) {
      \Drupal::logger('commerce_squad')->error($e->getMessage());
      return new Response('Error', Response::HTTP_INTERNAL_SERVER_ERROR);
    }
  }
}
 