<?php

namespace Drupal\commerce_squad\PluginForm;

use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Drupal\commerce_payment\PluginForm\PaymentOffsiteForm as BasePaymentOffsiteForm;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\Core\Form\FormStateInterface;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Drupal\Core\Url;

class SquadForm extends BasePaymentOffsiteForm {

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    // Gather necessary information for the payment transaction.
    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment = $this->entity;

    // Payment gateway configuration data.
    /** @var \Drupal\commerce_squad\Plugin\Commerce\PaymentGateway\SquadInterface $payment_gateway_plugin */
    $payment_gateway_plugin = $payment->getPaymentGateway()->getPlugin();
    $config = $payment_gateway_plugin->getConfiguration(); 
    $merchantId = $config['merchant_id'];
    $privateKey = $config['private_key'];
    $apiUrl = $config['mode'] === 'live' ? $config['live_url'] : $config['sandbox_url'];
    
    // Order and billing address.
    /** @var \Drupal\commerce_order\Entity\OrderInterface $order */
    $order = $payment->getOrder();

    // Adds information about the billing profile.
    if ($billing_profile = $order->getBillingProfile()) {
      /** @var \Drupal\address\AddressInterface $address */
      $address = $billing_profile->get('address')->first();
      $fields = [
        [
          'display_name' => 'Billing First Name',
          'variable_name' => 'first_name',
          'value' => $address->getGivenName(),
        ],
        [
          'display_name' => 'Billing Surname',
          'variable_name' => 'last_name',
          'value' => $address->getFamilyName(),
        ]
      ];
    }

    // Payment data.
    $transactionData = [
      'email' => $order->getEmail(),
      'amount' => $payment->getAmount()->getNumber() * 100, // Convert to kobo.
      'currency' => $payment->getAmount()->getCurrencyCode(),
      'customer_name' => $address->getGivenName() . ' ' . $address->getFamilyName(),
      'initiate_type' => 'inline',
      'transaction_ref' => $order->id(),
      'payment_channels' => ['card', 'bank', 'ussd', 'transfer'],
    ];

    // Initialize Guzzle HTTP client.
    $client = new \GuzzleHttp\Client();

    // Retry loop.
    for ($attempt = 1; $attempt <= 3; $attempt++) {
        try {
            // Make the HTTP request to initiate the transaction.
            $response = $client->post("$apiUrl/transaction/initiate", [
                'headers' => [
                    'Authorization' => 'Bearer ' . $privateKey,
                    'Content-Type' => 'application/json',
                ],
                'json' => $transactionData,
            ]);

            $responseData = json_decode($response->getBody()->getContents(), true);

            // Check if the transaction initiation was successful.
            if ($response->getStatusCode() === 200) {
                $checkoutUrl = $responseData['data']['checkout_url'];

                // Generate a secure token.
                $pkToken = \Drupal::service('csrf_token')->get('squad_form_token');

                // Store the private key in a session variable associated with the token.
                \Drupal::service('session')->set('squad_form_token_private_key', $privateKey);

                // Store the token in session for later retrieval.
                \Drupal::service('session')->set('squad_form_token', $pkToken);

                // Store relevant transaction data in the order object.
                $order->setData('squad', [
                    'transaction_ref' => $transactionData['transaction_ref'],
                    'checkout_url' => $checkoutUrl,
                ]);
                $order->save();


                // Construct a form with necessary data for redirection to Squad payment page.
                $data = [
                    'total' => $payment->getAmount()->getNumber(),
                ];
                $redirectMethod = BasePaymentOffsiteForm::REDIRECT_GET;
                $form = $this->buildRedirectForm($form, $form_state, $checkoutUrl, $data, $redirectMethod);

                return $form;
            } else {
                throw new PaymentGatewayException('Failed to initiate transaction with Squad.');
            }
        } catch (RequestException $e) {
            if ($attempt < 3) {
                // Log the error and retry after a short delay.
                \Drupal::logger('commerce_squad')->warning('Error communicating with Squad API. Retrying request...');
                sleep(1);
            } else {
                // If the maximum number of attempts is reached, throw an exception.
                throw new PaymentGatewayException('Failed to communicate with Squad API after multiple attempts.');
            }
            //TODO 
            //This works, but a more graceful redirection is needed if connection to Squad fails. 
        }
    }
  }
}
