README file for Commerce Squad

INTRODUCTION
------------
The Commerce Squad module integrates the Squad payment gateway seamlessly into Drupal Commerce, allowing merchants to accept payments globally. Squad provides a reliable payment solution, enabling merchants to expand their customer base and streamline their checkout process.

REQUIREMENTS
------------
This module requires the following components:
- Drupal Commerce package and its dependencies.
- [Squad Live Account](https://dashboard.squad.com/sign-up) for production environment testing and transactions.
- [Squad Sandbox Account](https://sandbox.squad.com/sign-up) for testing purposes.

INSTALLATION
------------
1. Download and install the Commerce Squad module as you would any other Drupal module. See [Installing modules (Drupal 8) documentation page](https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules) for further information.
2. Ensure that all dependencies, including the Squad PHP Library, are properly installed.
3. Configure the Squad payment methods in the Drupal Commerce admin interface.

CONFIGURATION
-------------
Once installed, configure the Squad payment gateway by following these steps:
1. Navigate to the [Payment gateways configuration page](/admin/commerce/config/payment-gateways) in the Drupal Commerce admin interface.
2. Enable the Squad payment methods.
3. Set the transaction mode to "Live" and enter your [Squad Live private key](https://dashboard.squad.com/) for production environment transactions.
4. For testing purposes, set the transaction mode to "Sandbox" and enter your [Squad Sandbox private key](https://sandbox.squad.com/) for sandbox environment transactions.

HOW IT WORKS
------------
General Workflow:
- Merchants must have separate Squad accounts for live and sandbox environments.
- After payment confirmation on Squad's side, customers are redirected back to the merchant's website to complete the checkout process.

TROUBLESHOOTING
---------------
If you encounter any issues during installation or configuration, please refer to the module's documentation or seek assistance from the maintainers.


MAINTAINERS
-----------
This project has been developed by [Chukwuemeka Munachi](https://www.drupal.org/u/cnario).
